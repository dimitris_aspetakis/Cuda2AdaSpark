# Cuda2AdaSpark

**Cuda2AdaSpark** is a CUDA-C to Ada SPARK transpiler. It uses the custom Frontc  module from [TASA_CIL](https://gitlab.bsc.es/lkosmidi/tasa_cil) that supports CUDA-C parsing, extends it with custom `#pragma` directives, and with the help of a custom Ada SPARK abstract syntax tree and a translation layer between the two ASTs, it is able to generate deployable code using Adacore's experimental CUDA backend for Ada.



## CUDA-C language restrictions

- TODO

  

## Directives aiding transpilation

- TODO



## Installation
- TODO



## Usage
- TODO



## Roadmap
- TODO



## Contributing
- TODO



## Authors and acknowledgment
- TODO



## License
- TODO
