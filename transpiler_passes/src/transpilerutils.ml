open Adaabs
open Adaabsvisit
open Str

let filter_map f lst =
  let rec aux acc = function
    | [] -> List.rev acc
    | x :: xs ->
      match f x with
      | Some y -> aux (y :: acc) xs
      | None -> aux acc xs
  in
  aux [] lst


class castTransformer = object(self)
  inherit nopAdaabsVisitor as super

  val vars : ada_var list ref = ref []

  method vdef (d : ada_definition) : ada_definition list visitAction =
  begin
    match d with
    | FunDef (target, name, attrs, args, decls, body) ->
      vars := args;
      vars := !vars @ filter_map (fun (id, (_, typ)) -> match typ with
        | NamedType str -> begin
          try
            let regexp = Str.regexp "_Array_" in
            let index = Str.search_forward regexp str 0 in
            Some (("__indexed__" ^ id, ([], NamedType (String.sub str 0 index))))
          with Not_found -> None
          end
        | _ -> None) args;
      List.iter (fun d -> match d with
        | Adaabs.VarDef  (idents, typ, _)
        | Adaabs.VarDecl (idents, typ) ->
          begin match typ with
          | (_, Array (_, elem_typ, ElemTyp)) ->
            vars := !vars @ List.map (fun id -> (id, typ)) idents;
            vars := !vars @ List.map (fun id -> ("__indexed__" ^ id, ([], elem_typ))) idents;
          | _ ->
            vars := !vars @ List.map (fun id -> (id, typ)) idents;
          end
        | _ -> ()
      ) decls;
      DoChildren
    | _ -> DoChildren
  end

  method vexpr (e : ada_expr) : ada_expr visitAction =
  begin
    match e with
    | BinaryExpr (lval, Assign, expr) -> begin
      match lval with
      | VarExpr id ->
        begin
          match List.find_opt (fun (v_id, _) -> id = v_id) !vars
          with
          | Some (_, (_, typ)) ->
            ChangeTo (
              BinaryExpr (lval, Assign, CastExpr (typ, expr))
            )
          | None -> DoChildren
        end
      | IndexExpr (VarExpr id, _) ->
        let id = "__indexed__" ^ id in
        begin
          match List.find_opt (fun (v_id, _) -> id = v_id) !vars
          with
          | Some (_, (_, typ)) ->
            ChangeTo (
              BinaryExpr (lval, Assign, CastExpr (typ, expr))
            )
          | None -> DoChildren
        end
      | _ -> DoChildren
     end
    | _ -> DoChildren
  end
end


(* TODO: fix when pure functions are supported *)
class assignCallTransformer = object(self)
  inherit nopAdaabsVisitor as super

  method vexpr (e : ada_expr) : ada_expr visitAction =
  begin
    match e with
    | BinaryExpr (
        VarExpr (var_name) as var_expr, Assign, ProcedureCall (id, expr_lst)
      ) -> ChangeTo (ProcedureCall (id, expr_lst @ [var_expr]))
    | _ -> DoChildren
  end
end


class returnStmntTransformer = object(self)
  inherit nopAdaabsVisitor as super

  method vdef (d : ada_definition) : ada_definition list visitAction =
  begin
    match d with
    | FunDef (target, name, attrs, args, decls, body) ->
      let (return_arg_name, (_, return_typ)) =
        List.fold_left (fun acc (ident, typ_decl) -> (ident, typ_decl))
          ("", ([], BaseType (VoidTyp))) args
      in
      let body' = filter_map (fun st -> match st with
        | Return expr when args <> [] ->
          Some (
            Computation (
              BinaryExpr (
                VarExpr (return_arg_name), Assign, CastExpr (return_typ, expr)
              )
            )
          )
        | Return expr when args = [] -> None
        | _ -> Some (st)
      ) body in
      ChangeTo (
        [FunDef (target, name, attrs, args, decls, body')]
      )
    | _ -> DoChildren
  end
end


class fixDeviceDeclsTransformer = object(self)
  inherit nopAdaabsVisitor as super

  method vexpr (e : ada_expr) : ada_expr visitAction =
  begin
    match e with
    | KernelCall (ident, dims, args) ->
      let args' = List.map (fun arg -> match arg with
        | VarExpr v_name when (try ((String.sub v_name 0 2) = "d_")
          with Invalid_argument _ -> false) ->
          begin
            try VarExpr (String.sub v_name 2 (String.length v_name - 2))
            with Invalid_argument _ -> arg
          end
        | _ -> arg
      ) args in
      ChangeTo (
        KernelCall (ident, dims, args')
      )
    | _ -> DoChildren
  end

  method vdef (d : ada_definition) : ada_definition list visitAction =
  begin
    match d with
    | VarDef (ident_lst, td, expr) ->
      let ident_lst' =
        List.filter (fun id ->
          try String.sub id 0 2 <> "d_"
          with Invalid_argument _ -> true) ident_lst
      in
      if ident_lst' <> [] then
        ChangeTo (
          [VarDef (ident_lst', td, expr)]
        )
      else
        ChangeTo ([])
    | VarDecl (ident_lst, td) ->
      let ident_lst' =
        List.filter (fun id ->
          try String.sub id 0 2 <> "d_"
          with Invalid_argument _ -> true) ident_lst
      in
      if ident_lst' <> [] then
        ChangeTo (
          [VarDecl (ident_lst', td)]
        )
      else
        ChangeTo ([])
    | _ -> DoChildren
  end
end


class cudaIndexVarTransformer = object(self)
  inherit nopAdaabsVisitor as super

  method vexpr (e : ada_expr) : ada_expr visitAction =
  begin
    match e with
    | MemberOfExpr (VarExpr (var), field) ->
      ChangeTo (
        match var with
        | "threadIdx" ->
          MemberOfExpr (VarExpr ("Thread_Idx"), String.uppercase_ascii field)
        | "blockIdx" ->
          MemberOfExpr (VarExpr ("Block_Idx"), String.uppercase_ascii field)
        | "blockDim" ->
          MemberOfExpr (VarExpr ("Block_Dim"), String.uppercase_ascii field)
        | _ -> e
      )
    | _ -> DoChildren
  end
end


let rec find_idx_args (expr : ada_expr) : ada_expr list =
  let args : ada_expr list ref = ref [] in
  begin match expr with
  | MemberOfExpr (VarExpr (var), field) ->
    begin match var with
    | "Thread_Idx" ->
      args := !args @ [
        MemberOfExpr (VarExpr ("Thread_Idx"), field)
      ]
    | "Block_Idx" ->
      args := !args @ [
        MemberOfExpr (VarExpr ("Block_Idx"), field)
      ]
    | "Block_Dim" ->
      args := !args @ [
        MemberOfExpr (VarExpr ("Block_Dim"), field)
      ]
    | _ -> ()
    end
  | UnaryExpr (un_op, un_expr) ->
    args := !args @ find_idx_args un_expr
  | BinaryExpr (expr1, bin_op, expr2) ->
    args := !args @ (find_idx_args expr1) @ (find_idx_args expr2)
  | ParenExpr (child_expr) ->
    args := !args @ find_idx_args child_expr
  | CastExpr (typ, child_expr) ->
    args := !args @ find_idx_args child_expr
  | IndexExpr (arr_expr, idx_expr_list) ->
    args := !args @ List.fold_left (fun acc expr -> acc @ find_idx_args expr) [] idx_expr_list
  | TupleExpr (expr_list) ->
    args := !args @ List.fold_left (fun acc expr -> acc @ find_idx_args expr) [] expr_list
  | CompoundExpr init_list ->
    args := !args @ List.fold_left (fun acc (_, expr) -> acc @ find_idx_args expr) [] init_list
  | _ -> ()
  end;
  !args

let rec transform_idx_expr (expr : ada_expr) : ada_expr =
  begin match expr with
  | MemberOfExpr (VarExpr (var), field) ->
    begin match var with
    | "Thread_Idx" -> VarExpr (var)
    | "Block_Idx" -> VarExpr (var)
    | "Block_Dim" -> VarExpr (var)
    | _ -> expr
    end
  | UnaryExpr (un_op, un_expr) ->
    UnaryExpr (un_op, transform_idx_expr (un_expr))
  | BinaryExpr (expr1, bin_op, expr2) ->
    BinaryExpr (transform_idx_expr expr1, bin_op, transform_idx_expr expr2)
  | ParenExpr (child_expr) ->
    ParenExpr (transform_idx_expr child_expr)
  | CastExpr (typ, child_expr) ->
    CastExpr (typ, transform_idx_expr child_expr)
  | IndexExpr (arr_expr, idx_expr_list) ->
    IndexExpr (transform_idx_expr arr_expr, List.map transform_idx_expr idx_expr_list)
  | TupleExpr (expr_list) ->
    TupleExpr (List.map transform_idx_expr expr_list)
  | CompoundExpr init_list -> CompoundExpr (
      List.map (fun (init_what, expr) ->
        (init_what, transform_idx_expr expr)
      ) init_list
    )
  | _ -> expr
  end


class cudaIndexFuncTransformer = object(self)
  inherit nopAdaabsVisitor as super

  method vdef (d : ada_definition) : ada_definition list visitAction =
  begin
    match d with
    | FunDef (Global, name, attrs, args, decls, body) ->
      (* TODO: add support for multiple indexing definitions, all assigning to
       * a variable that starts with `cuda_idx` *)
      let idx_args_lst : ada_expr list list ref = ref [] in
      let idx_expr_lst : ada_expr list ref = ref [] in

      let decls' = List.map (fun decl -> match decl with
        | VarDef (["idx"], typ_decl, expr) ->
          idx_args_lst := find_idx_args expr :: !idx_args_lst;
          idx_expr_lst := expr :: !idx_expr_lst;
          VarDecl (["idx"], typ_decl)
        | _ -> decl
      ) decls in

      let idx_calls = List.map (fun idx_args ->
        let args = idx_args @ [VarExpr ("idx")] in
        Computation (ProcedureCall ("Cuda_Index", args))
      ) !idx_args_lst in

      let idx_fun_defs = List.map2 (fun idx_expr idx_args ->
        let args = filter_map (fun expr -> match expr with
          | MemberOfExpr (VarExpr (var), field) ->
            Some (var, ([], NamedType ("unsigned")))
          | _ -> None
        ) idx_args @ [
          ("idx", ([Out], BaseType (Discrete (NaturalTyp))))
        ] in

        FunDef (Host, "Cuda_Index", [SparkMode (Off)], args, [], [
          Computation (
            BinaryExpr (VarExpr "idx", Assign, transform_idx_expr idx_expr)
          )
        ])
      ) !idx_expr_lst !idx_args_lst in

      ChangeTo (
        idx_fun_defs @
        [FunDef (Global, name, attrs, args, decls', idx_calls @ body)]
      )
    | _ -> DoChildren
  end
end


class wrapperTransformer = object(self)
  inherit nopAdaabsVisitor as super

  method vexpr (e : ada_expr) : ada_expr visitAction =
  begin
    match e with
    | KernelCall (ident, (grid_dim, block_dim), args) ->
      let wrapper_name = ident ^ "_Wrapper" in
      let wrapper_args = grid_dim :: block_dim :: args in
      ChangeTo (
        ProcedureCall (wrapper_name, wrapper_args)
      )
    | _ -> DoChildren
  end

  method vdef (d : ada_definition) : ada_definition list visitAction =
  begin
    let ends_with_suffix str suffix =
      let str_len = String.length str in
      let suffix_len = String.length suffix in
      str_len >= suffix_len &&
      (String.sub str (str_len - suffix_len) suffix_len) = suffix
    in
    match d with
    | FunDef (Global, name, attrs, args, decls, body) ->
      (********************* Create wrapper's arguments **********************)
      let wrapper_args = [
        ("gridDim", ([], NamedType "dim3"));
        ("blockDim", ([], NamedType "dim3"))
      ] @ List.map (fun arg ->
        let rec keep_inout = function
          | InOut :: _ -> [InOut]
          | Out :: _ -> [Out]
          | In :: _ -> [In]
          | _ :: rest -> keep_inout rest
          | [] -> []
        in
        match arg with
        | (ident, (attrs, NamedType typ_name))
          when ends_with_suffix typ_name "_Device_Access" ->
          (ident, (keep_inout attrs,
            NamedType (String.sub typ_name 0 (String.length typ_name - 14))))
        | _ -> arg
      ) args in

      (********************* Create wrapper's attributes *********************)
      (* FIXME: properly add the implicit precondition on 'First being zero for
       * kernel arrays
       *)
      let wrapper_attrs =
        let rec take_until pred xs =
          match xs with
          | [] -> []
          | x :: xs ->
            if pred x then
              x :: take_until pred xs
            else
              []
        in
        let is_pre_pragma = function
          | PragmaDef (Pre _) -> true
          | _ -> false
        in
        let pre_attrs = take_until is_pre_pragma decls in
        match pre_attrs with
        | [] -> []
        | [PragmaDef (Pre (expr))] -> [PreAttr (expr)]
        | PragmaDef (Pre (expr)) :: rest ->
          [PreAttr (
            List.fold_left (fun acc pd ->
              begin match pd with
              | PragmaDef (Pre (expr)) -> BinaryExpr (acc, And, expr)
              | _ -> raise (Invalid_argument "Expected Pre PragmaDef")
              end
            ) expr rest
          )]
        | _ -> raise (Invalid_argument "Expected Pre PragmaDef")
      in

      (***************** Preparation work for the arguments ******************)
      let device_args = List.filter (fun arg -> match arg with
        | (_, (_, NamedType typ_name))
          when ends_with_suffix typ_name "_Device_Access" -> true
        | _ -> false
      ) args in
      
      let in_args : ada_var list ref = ref [] in
      let inout_args : ada_var list ref = ref [] in
      let out_args : ada_var list ref = ref [] in
      let rec is_inout = function
        | inout :: rest -> inout = InOut || (is_inout rest)
        | [] -> false
      in
      let rec is_out = function
        | inout :: rest -> inout = Out || (is_out rest)
        | [] -> false
      in
      List.iter (fun arg -> match arg with
        | (ident, (inout, NamedType typ_name)) when is_inout inout ->
          inout_args := !inout_args @ [arg]
        | (ident, (inout, NamedType typ_name)) when is_out inout ->
          out_args := !out_args @ [arg]
        | (ident, (_, NamedType typ_name)) ->
          in_args := !in_args @ [arg]
        | _ ->
          raise (Invalid_argument "Expected NamedType")
      ) device_args;

      let _distinct_types : ada_type list =
        List.map (fun (_, (_, typ)) -> typ) device_args |>
        List.fold_left (
          fun acc x -> if List.mem x acc then acc else x :: acc
        ) []
      in

      (******************** Create wrapper's declarations ********************)
      (* TODO: add the generation of the `Free` procedures *)
      let wrapper_decls =
        List.map (fun (arg_name, (_, typ)) -> match typ with
          | NamedType (typ_str) ->
            let host_typ_name = 
              String.sub typ_str 0 (String.length typ_str - 14)
            in
            VarDef (
              ["D_" ^ arg_name],
              ([], typ),
              AllocExpr (host_typ_name, [MemberOfExpr (VarExpr (arg_name), "range")])
            )
          | _ ->
            raise (Invalid_argument "Expected NamedType")
        ) device_args
      in

      (************************ Create wrapper's body ************************)
      (* TODO: add calls to free all allocated device variables *)
      let wrapper_body =
        List.map (fun (arg_name, _) ->
          Computation (
            BinaryExpr (
              MemberOfExpr (VarExpr ("D_" ^ arg_name), "all"),
              Assign,
              VarExpr (arg_name)
            )
          )
        ) (!in_args @ !inout_args) @ [Computation (KernelCall (
            name,
            (
              TupleExpr [
                MemberOfExpr (VarExpr "gridDim", "x");
                MemberOfExpr (VarExpr "gridDim", "y");
                MemberOfExpr (VarExpr "gridDim", "z")
              ],
              TupleExpr [
                MemberOfExpr (VarExpr "blockDim", "x");
                MemberOfExpr (VarExpr "blockDim", "y");
                MemberOfExpr (VarExpr "blockDim", "z")
              ]
            ), 
            List.map (fun (arg_name, (_, typ)) -> match typ with
              | NamedType (typ_name) ->
                if ends_with_suffix typ_name "_Device_Access" then
                  VarExpr ("D_" ^ arg_name)
                else
                  VarExpr (arg_name)
              | _ ->
                VarExpr (arg_name)
            ) args
        ))] @
        List.map (fun (arg_name, _) ->
          Computation (
            BinaryExpr (
              VarExpr (arg_name),
              Assign,
              MemberOfExpr (VarExpr ("D_" ^ arg_name), "all")
            )
          )
        ) (!inout_args @ !out_args)
      in

      let args' = List.map (fun arg -> match arg with
        | (ident, (_, (NamedType typ_name as typ)))
          when ends_with_suffix typ_name "_Device_Access" ->
          (ident, ([NotNull], typ))
        | _ -> arg
      ) args in

      let decls' = List.map (fun decl -> match decl with
        | PragmaDef (Pre (expr)) -> PragmaDef (Assume (expr))
        | _ -> decl
      ) decls in

      ChangeTo ([
        FunDef (Global, name, attrs, args', decls', body);
        FunDef (
          Host,
          name ^ "_Wrapper",
          wrapper_attrs,
          wrapper_args,
          wrapper_decls,
          wrapper_body
        )
      ])
    | _ -> DoChildren
  end
end
