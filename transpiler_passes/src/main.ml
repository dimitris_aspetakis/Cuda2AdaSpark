module F = Frontc
module C = Cil
module E = Errormsg

module O = Ciltutoptions


let main () =
  let usageMsg = "Usage: ciltutcc [options] source-files" in
  Arg.parse (O.align ()) Ciloptions.recordFile usageMsg;

  Ciloptions.fileNames := List.rev !Ciloptions.fileNames;

  let cabs = F.parse_to_cabs (List.hd !Ciloptions.fileNames) in
  Cuda2ada.transpile cabs
;;


begin 
  try 
    main () 
  with
  | E.Error -> ()
end;
exit (if !E.hadErrors then 1 else 0)
