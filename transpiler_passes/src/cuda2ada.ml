module E = Errormsg
open Cabs
open Adaabs
open Adaabsvisit
open Transpiler
open Transpilerutils

(****************************** Helper globals *******************************)
let filename : string ref = ref "adacode"

let out : out_channel ref = ref stdout

let setOutput out_chan =
  out := out_chan

let print str = output_string !out str

let print_ada_def (ident : int) (d : ada_definition) =
  print (string_of_ada_def d ident)

let ends_with_suffix str suffix =
  let str_len = String.length str in
  let suffix_len = String.length suffix in
  str_len >= suffix_len &&
  (String.sub str (str_len - suffix_len) suffix_len) = suffix
(*****************************************************************************)



let transpile ((fname, file) : Cabs.file) : unit =
  filename := (String.sub fname 2 (String.length fname - 4));
  E.log "\nFilename: %s\n\n" !filename;

  (********************* Declare any visitor passes here *********************)
  let assignCallTransform =
    (* Fix some Procedure call assignments *)
    visitAdaabsDefList (new assignCallTransformer :> adaabsVisitor) in
  let castTransform =
    (* Add casts to all assignment statements with the lvalue's type *)
    visitAdaabsDefList (new castTransformer :> adaabsVisitor) in
  let returnStmntTransform =
    (* Change all return statements to assignments since all functions get
     * transpiled to procedures *)
    visitAdaabsDefList (new returnStmntTransformer :> adaabsVisitor) in
  let fixDeviceDeclsTransform =
    (* Remove all variable declarations for device variables — that is
     * everything starting with `d_` *)
    visitAdaabsDefList (new fixDeviceDeclsTransformer :> adaabsVisitor) in
  let cudaIndexVarTransform =
    (* Change all threadIdx, blockIdx and blockDim to their Ada equivalents *)
    visitAdaabsDefList (new cudaIndexVarTransformer :> adaabsVisitor) in
  let cudaIndexFuncTransform =
    (* Generate a procedure with SPAKR_Mode => Off for indexing *)
    visitAdaabsDefList (new cudaIndexFuncTransformer :> adaabsVisitor) in
  let wrapperTransform =
    (* Generate and call wrappers for every kernel and kernel call *)
    visitAdaabsDefList (new wrapperTransformer :> adaabsVisitor) in

  let c_defs = group_fundef_pragmas file in

  let ada_defs =
    List.filter (
      fun d -> match d with
        | FUNDEF _ -> true
        | _ -> false
    ) c_defs |> List.map transpile_def |> List.flatten |>
    assignCallTransform |>
    castTransform |>
    returnStmntTransform |>
    fixDeviceDeclsTransform |>
    cudaIndexVarTransform |>
    cudaIndexFuncTransform |>
    wrapperTransform
  in

  let kernel_decls : ada_definition list ref = ref [] in
  let kernel_defs : ada_definition list ref = ref [] in
  let kernel_wrapper_decls : ada_definition list ref = ref [] in
  let kernel_wrapper_defs : ada_definition list ref = ref [] in
  let host_defs : ada_definition list ref = ref [] in

  List.iter (fun def -> match def with
    | FunDef (Global, name, attrs, args, _, _) ->
      kernel_decls := !kernel_decls @ [FunDecl (Global, name, attrs, args)];
      kernel_defs := !kernel_defs @ [def];
    | FunDef (target, name, attrs, args, _, _) when name = "Cuda_Index" ->
      let decl_attrs =
        SparkMode (On) :: List.filter (fun f_attr -> match f_attr with
          | SparkMode _ -> false
          | _ -> true
        ) attrs
      in
      kernel_decls := !kernel_decls @ [FunDecl (target, name, decl_attrs, args)];
      kernel_defs := !kernel_defs @ [def];
    | FunDef (target, name, attrs, args, _, _) when ends_with_suffix name "_Wrapper" ->
      kernel_wrapper_decls :=
        !kernel_wrapper_decls @ [FunDecl (target, name, attrs, args)];
      kernel_wrapper_defs := !kernel_wrapper_defs @ [def];
    | FunDef _ ->
      host_defs := !host_defs @ [def];
    | _ -> raise (InternalTranspilerError "Expected function definition")
  ) ada_defs;
  (***************************************************************************)


  (**** Necessary prelude — type definitions for the rest of the program *****)
  let prelude = [
    TypeDef (SubType (
      "CudaIdx",
      BaseType (Discrete (NaturalTyp)),
      Some (ConstantExpr (Int (0))),
      Some (ConstantExpr (Int (1023))),
      []
    ));
    TypeDef (FullType (
      "dim3",
      Record ([(["x"; "y"; "z"], ([], NamedType ("CudaIdx")))]),
      []
    ));
    (* FIXME: the following variable declarations are probably is unecessary *)
    VarDecl (
      ["threadIdx"; "blockIdx"; "blockDim"],
      ([], NamedType ("dim3"))
    )
  ] @ List.map (fun td -> TypeDef (td)) !ada_typedefs in


  (******************************* kernels.ads *******************************)
  setOutput (open_out ("kernels.ads"));
  print ("with CUDA.Storage_Models; use CUDA.Storage_Models;\n" ^
         "with CUDA.Vector_Types;   use CUDA.Vector_Types;\n" ^
         "with Interfaces.C;        use Interfaces.C;\n\n" ^
         "package Kernels with\n" ^
         "  SPARK_Mode\n" ^
         "is\n\n");

  (* Print Kernel procedure specifications and the prelude *)
  List.iter (print_ada_def 3) prelude; print "\n";
  List.iter (print_ada_def 3) !kernel_decls;

  print ("\nend Kernels;\n");
  (******************************* Package End *******************************)


  (******************************* kernels.adb *******************************)
  setOutput (open_out ("kernels.adb"));
  print ("with CUDA.Runtime_Api; use CUDA.Runtime_Api;\n" ^
         "with Interfaces.C;     use Interfaces.C;\n\n" ^
         "package body Kernels with\n" ^
         "  SPARK_Mode\n" ^
         "is\n");

  (* Print Kernel procedure definitions *)
  List.iter (print_ada_def 3) !kernel_defs;

  print ("\nend Kernels;\n");
  (******************************* Package End *******************************)


  (*************************** kernel_wrappers.ads ***************************)
  setOutput (open_out ("kernel_wrappers.ads"));
  print ("with Interfaces.C; use Interfaces.C;\n" ^
         "with Kernels;      use Kernels;\n\n" ^
         "package Kernel_Wrappers with\n" ^
         "  SPARK_Mode\n" ^
         "is\n");

  (* Print Kernel_Wrapper procedure specifications *)
  List.iter (print_ada_def 3) !kernel_wrapper_decls;

  print ("\nend Kernel_Wrappers;\n");
  (******************************* Package End *******************************)


  (*************************** kernel_wrappers.adb ***************************)
  setOutput (open_out ("kernel_wrappers.adb"));
  print ("with CUDA.Runtime_Api; use CUDA.Runtime_Api;\n" ^
         "with Kernels;          use Kernels;\n\n" ^
         "package body Kernel_Wrappers with\n" ^
         "  SPARK_Mode => Off\n" ^
         "is\n");

  (* Print Kernel_Wrapper procedure definitions *)
  List.iter (print_ada_def 3) !kernel_wrapper_defs;

  print ("\nend Kernel_Wrappers;\n");
  (******************************* Package End *******************************)


  (******************************** main.adb *********************************)
  setOutput (open_out ("main.adb"));
  print ("with Kernels;          use Kernels;\n" ^
         "with Kernel_Wrappers;  use Kernel_Wrappers;\n\n");

  (* Print host procedures *)
  (* TODO: check what to do exactly if we have more host functions *)
  List.iter (print_ada_def 0) !host_defs;
  (******************************* Package End *******************************)


  E.log "----------------------- Transpilation Done -----------------------\n"
