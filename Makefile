.PHONY: all cil transpiler_passes clean

all: cil transpiler_passes

cil:
	cd cil && make install

transpiler_passes:
	cd transpiler_passes/build && make clean && make install

clean:
	cd cil && make clean
	cd transpiler_passes/build && make clean
	find tests \( -name "*.i" -o -name "*.o" -o -name "*.ali" -o -name "*.adb" -o -name "*.ads" \) -type f -delete
