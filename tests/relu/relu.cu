#define SIZE 3


#pragma Assume (a in -256 .. 255)
#pragma Assume (b in 0 .. 255)
#pragma InArgs (a, size)
#pragma OutArgs (b)
#pragma Pre (a.last == size - 1 && a.last == b.last)
__global__ void reluKernel(int *a, unsigned *b, unsigned size) {
    unsigned idx = threadIdx.x + blockIdx.x * blockDim.x;

    // Check if the thread ID is within the array bounds
    if (idx < size) {
        if (a[idx] > 0)
            b[idx] = a[idx];
        else
            b[idx] = 0;
        #pragma Assert (b[idx] >= 0)
    }
}


#pragma Assume (a in -256 .. 255)
#pragma Assume (b in 0 .. 255)
int main(void) {
    // host copies of a, b, c
    int a[SIZE] = {-37, 12, -4};
    unsigned b[SIZE];

    // device copies of a, b
    int *d_a;
    unsigned *d_b;

    // Alloc space for device copies of a, b
    // REMINDER: this can't currently be checked, but Ada code will be correct
    // CHECK: Maybe do a pass to check that sizeof (typ) has the correct type, and
    // then add an Assertion like on cudaMemcpy
    cudaMalloc((void **)&d_a, SIZE * sizeof (int));
    cudaMalloc((void **)&d_b, SIZE * sizeof (unsigned));

    // Copy inputs to device
    cudaMemcpy(d_a, a, SIZE, cudaMemcpyHostToDevice);

    // Launch kernel on GPU with 3 threads
    dim3 gridDim = {1, 1, 1};
    dim3 blockDim = {SIZE, 1, 1};
    reluKernel<<<gridDim, blockDim>>>(d_a, d_b, SIZE);

    // Copy result back to host
    cudaMemcpy(b, d_b, SIZE, cudaMemcpyDeviceToHost);

    // Cleanup
    cudaFree(d_a); cudaFree(d_b);
    return 0;
}
