#define SIZE 3


#pragma Assume (a in 0 .. 10)
#pragma Assume (b in 0 .. 10)
#pragma Assume (c in 0 .. 20)
#pragma InArgs (a, b, size)
#pragma OutArgs (c)
#pragma Pre (a.last == size - 1 && a.last == b.last && b.last == c.last)
__global__ void kernel(unsigned *a, unsigned *b, unsigned *c, unsigned size) {
    unsigned idx = threadIdx.x + blockIdx.x * blockDim.x;

    // Forget to check if the element we are dividing with is non-zero
    if (idx < size) {
        c[idx] = a[idx] / b[idx];
    }
}


#pragma Assume (a in 0 .. 10)
#pragma Assume (b in 0 .. 10)
#pragma Assume (c in 0 .. 20)
int main(void) {
    // host copies of a, b, c
    unsigned a[SIZE] = {1, 10, 9};
    unsigned b[SIZE] = {5, 10, 8};
    unsigned c[SIZE];

    unsigned *d_a, *d_b, *d_c; // device copies of a, b, c

    // Alloc space for device copies of a, b, c
    // REMINDER: this can't currently be checked, but Ada code will be correct
    // CHECK: Maybe do a pass to check that sizeof (typ) has the correct type, and
    // then add an Assertion like on cudaMemcpy
    cudaMalloc((void **)&d_a, SIZE * sizeof (unsigned));
    cudaMalloc((void **)&d_b, SIZE * sizeof (unsigned));
    cudaMalloc((void **)&d_c, SIZE * sizeof (unsigned));

    // Copy inputs to device
    cudaMemcpy(d_a, a, SIZE, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, SIZE, cudaMemcpyHostToDevice);

    // Launch kernel on GPU with 3 threads
    dim3 gridDim = {1, 1, 1};
    dim3 blockDim = {SIZE, 1, 1};
    kernel<<<gridDim, blockDim>>>(d_a, d_b, d_c, SIZE);

    // Copy result back to host
    cudaMemcpy(c, d_c, SIZE, cudaMemcpyDeviceToHost);

    // Cleanup
    cudaFree(d_a); cudaFree(d_b); cudaFree(d_c);
    return 0;
}
