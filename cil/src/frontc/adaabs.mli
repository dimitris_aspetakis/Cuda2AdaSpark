exception InternalTranspilerError of string


(*****************************************************************************
 * Ada Abstract Syntax Tree                                                  *
 *****************************************************************************)
type ada_ident = string

and ada_discrete_type =
  | IntegerTyp  (* ..., -2, -1, 0, 1, 2, ... *)
  | NaturalTyp  (*              0, 1, 2, ... *)
  | PositiveTyp (*                 1, 2, ... *)

and ada_real_type =
  | FloatTyp

and ada_base_type =
  | VoidTyp
  | Discrete of ada_discrete_type
  | Real of ada_real_type
  | StringTyp

and ada_range =
  | Closed of ada_expr * ada_expr
  | Open of ada_discrete_type

and arr_type_variant =
  | ElemTyp
  | ArrTyp

(* TODO: maybe make Array recursive if you can fix printing *)
and ada_type =
  | BaseType of ada_base_type
  | Record of (ada_ident list * ada_type_decl) list
  | Array of ada_range list * ada_type * arr_type_variant
  | Access of ada_type
  | NamedType of string

(* TODO: maybe ensure these are put them only in args *)
and ada_type_attr =
  | In | Out | InOut
  | NotNull

and ada_type_decl = ada_type_attr list * ada_type

and ada_var = ada_ident * ada_type_decl

and ada_constant =
  | Int of int
  | Float of float
  | String of string

and ada_unary_op =
  | Minus | Plus | Not

and ada_binary_op =
  | Add | Sub | Mul | Div | Mod
  | And | Or
  | Eq | Ne | Lt | Gt | Le | Ge
  | Assign

and init_what =
  | Nothing
  | InField of ada_ident * init_what

and ada_expr =
  | UnaryExpr of ada_unary_op * ada_expr
  | BinaryExpr of ada_expr * ada_binary_op * ada_expr
  | ParenExpr of ada_expr
  | CastExpr of ada_type * ada_expr
  | ConstantExpr of ada_constant
  | VarExpr of ada_ident
  (* TODO: maybe make IndexExpr recursive for easier transpiling of
   * multi-dimensional arrays *)
  | IndexExpr of ada_expr * ada_expr list
  | TupleExpr of ada_expr list
  | MemberOfExpr of ada_expr * string
  | CompoundExpr of (init_what * ada_expr) list
  (* TODO: move ProcedureCall and KernelCall to ada_stmnt, and possible add a
   * FunctionCall here *)
  | ProcedureCall of ada_ident * ada_expr list
  | KernelCall of ada_ident * (ada_expr * ada_expr) * ada_expr list
  | AllocExpr of string * ada_expr list

and ada_stmnt =
  | Computation of ada_expr
  | Block of ada_block
  | If of ada_expr * ada_block * ada_else
  | Return of ada_expr
  | Definition of ada_definition
  | Pragma of spark_pragma

and ada_else =
  | ElseStop
  | Elsif of ada_expr * ada_block * ada_else
  | Else of ada_block

and ada_decl_block = ada_definition list

and ada_block = ada_stmnt list

and ada_typedef_attr =
  | CudaStorageModel

and ada_typedef =
  | FullType of string * ada_type * ada_typedef_attr list
  | SubType of string * ada_type * ada_expr option * ada_expr option * ada_typedef_attr list

and ada_target =
  | Host
  | Device
  | Global

and ada_spark_mode =
  | Default
  | On
  | Off

and ada_func_attr =
  | PreAttr of ada_expr
  | PostAttr of ada_expr
  | Target of ada_target
  | SparkMode of ada_spark_mode

(*
 * TODO: Create and use a new ada_arg type for the fundef arguments — only that
 * type should have in/out attributes, and not ada_var
 *)
and ada_definition =
  | FunDef of ada_target * ada_ident * ada_func_attr list * ada_var list * ada_decl_block * ada_block
  | FunDecl of ada_target * ada_ident * ada_func_attr list * ada_var list
  | VarDef of ada_ident list * ada_type_decl * ada_expr
  | VarDecl of ada_ident list * ada_type_decl
  | TypeDef of ada_typedef
  | PragmaDef of spark_pragma

(* TODO: check if we might need attributes like SPARK_MODE *)
and ada_package = string * ada_definition list

(* Ada-SPARK pragmas *)
and spark_pragma =
  (****** Pragmas that don't have a pragma representation in Ada-SPARK *******)
  | ScalarRange of ada_ident * ada_expr option * ada_expr option
                                                       (* 'None' in any of the
                                                        * 'int option' fields 
                                                        * means open end *)
  | InArgs of ada_ident list
  | OutArgs of ada_ident list
  | InOutArgs of ada_ident list
  | Pre of ada_expr
  | Post of ada_expr
  (********* Pragmas that are translated to actual Ada-SPARK pragmas *********)
  | Assume of ada_expr
  | Assert of ada_expr


(*****************************************************************************
 * String representation of the Ada-SPARK AST                                *
 *****************************************************************************)
val indent_string: string -> int -> string


(*************************** Ada Types and Variables *************************)
val string_of_ada_discrete_type: ada_discrete_type -> string

val string_of_ada_real_type: ada_real_type -> string

val string_of_ada_base_type: ada_base_type -> string

val string_of_ada_range: ada_range -> string

val string_of_ada_type: ada_type -> string

val string_of_ada_type_attr: ada_type_attr -> string

val string_of_ada_type_decl: (ada_type_attr list * ada_type) -> string

val string_of_ada_var: (ada_ident * ada_type_decl) -> string


(****************************** Ada expressions ******************************)
val string_of_ada_constant: ada_constant -> string

val string_of_ada_unary_op: ada_unary_op -> string

val string_of_ada_binary_op: ada_binary_op -> string

val string_of_ada_expr: ada_expr -> string


(************************ Higher-level Ada Constructs ************************)
val string_of_ada_else: ada_else -> int -> string

val string_of_ada_stmnt: ada_stmnt -> int -> string

val string_of_ada_block: ada_block -> int -> string

val string_of_ada_decl_block: ada_decl_block -> int -> string

val string_of_ada_typedef: ada_typedef -> string

val string_of_ada_def: ada_definition -> int -> string

val string_of_ada_package: ada_package -> string


(******************************** Ada Pragmas ********************************)
val string_of_spark_pragma: spark_pragma -> string
