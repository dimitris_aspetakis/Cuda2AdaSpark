exception InternalTranspilerError of string


(*****************************************************************************
 * Ada Abstract Syntax Tree                                                  *
 *****************************************************************************)
type ada_ident = string

and ada_discrete_type =
  | IntegerTyp  (* ..., -2, -1, 0, 1, 2, ... *)
  | NaturalTyp  (*              0, 1, 2, ... *)
  | PositiveTyp (*                 1, 2, ... *)

and ada_real_type =
  | FloatTyp

and ada_base_type =
  | VoidTyp
  | Discrete of ada_discrete_type
  | Real of ada_real_type
  | StringTyp

and ada_range =
  | Closed of ada_expr * ada_expr
  | Open of ada_discrete_type

and arr_type_variant =
  | ElemTyp
  | ArrTyp

(* TODO: maybe make Array recursive if you can fix printing *)
and ada_type =
  | BaseType of ada_base_type
  | Record of (ada_ident list * ada_type_decl) list
  | Array of ada_range list * ada_type * arr_type_variant
  | Access of ada_type
  | NamedType of string

(* TODO: maybe ensure these are put them only in args *)
and ada_type_attr =
  | In | Out | InOut
  | NotNull

and ada_type_decl = ada_type_attr list * ada_type

and ada_var = ada_ident * ada_type_decl

and ada_constant =
  | Int of int
  | Float of float
  | String of string

and ada_unary_op =
  | Minus | Plus | Not

and ada_binary_op =
  | Add | Sub | Mul | Div | Mod
  | And | Or
  | Eq | Ne | Lt | Gt | Le | Ge
  | Assign

and init_what =
  | Nothing
  | InField of ada_ident * init_what

and ada_expr =
  | UnaryExpr of ada_unary_op * ada_expr
  | BinaryExpr of ada_expr * ada_binary_op * ada_expr
  | ParenExpr of ada_expr
  | CastExpr of ada_type * ada_expr
  | ConstantExpr of ada_constant
  | VarExpr of ada_ident
  (* TODO: maybe make IndexExpr recursive for easier transpiling of
   * multi-dimensional arrays *)
  | IndexExpr of ada_expr * ada_expr list
  | TupleExpr of ada_expr list
  | MemberOfExpr of ada_expr * string
  | CompoundExpr of (init_what * ada_expr) list
  (* TODO: move ProcedureCall and KernelCall to ada_stmnt, and possible add a
   * FunctionCall here *)
  | ProcedureCall of ada_ident * ada_expr list
  | KernelCall of ada_ident * (ada_expr * ada_expr) * ada_expr list
  | AllocExpr of string * ada_expr list

and ada_stmnt =
  | Computation of ada_expr
  | Block of ada_block
  | If of ada_expr * ada_block * ada_else
  | Return of ada_expr
  | Definition of ada_definition
  | Pragma of spark_pragma

and ada_else =
  | ElseStop
  | Elsif of ada_expr * ada_block * ada_else
  | Else of ada_block

and ada_decl_block = ada_definition list

and ada_block = ada_stmnt list

and ada_typedef_attr =
  | CudaStorageModel

and ada_typedef =
  | FullType of string * ada_type * ada_typedef_attr list
  | SubType of string * ada_type * ada_expr option * ada_expr option * ada_typedef_attr list

and ada_target =
  | Host
  | Device
  | Global

and ada_spark_mode =
  | Default
  | On
  | Off

and ada_func_attr =
  | PreAttr of ada_expr
  | PostAttr of ada_expr
  | Target of ada_target
  | SparkMode of ada_spark_mode

(*
 * TODO: Create and use a new ada_arg type for the fundef arguments — only that
 * type should have in/out attributes, and not ada_var
 *)
and ada_definition =
  | FunDef of ada_target * ada_ident * ada_func_attr list * ada_var list * ada_decl_block * ada_block
  | FunDecl of ada_target * ada_ident * ada_func_attr list * ada_var list
  | VarDef of ada_ident list * ada_type_decl * ada_expr
  | VarDecl of ada_ident list * ada_type_decl
  | TypeDef of ada_typedef
  | PragmaDef of spark_pragma

(* TODO: check if we might need attributes like SPARK_MODE *)
and ada_package = string * ada_definition list

(* Ada-SPARK pragmas *)
and spark_pragma =
  (****** Pragmas that don't have a pragma representation in Ada-SPARK *******)
  | ScalarRange of ada_ident * ada_expr option * ada_expr option
                                                       (* 'None' in any of the
                                                        * 'int option' fields 
                                                        * means open end *)
  | InArgs of ada_ident list
  | OutArgs of ada_ident list
  | InOutArgs of ada_ident list
  | Pre of ada_expr
  | Post of ada_expr
  (********* Pragmas that are translated to actual Ada-SPARK pragmas *********)
  | Assume of ada_expr
  | Assert of ada_expr


(*****************************************************************************
 * String representation of the Ada-SPARK AST                                *
 *****************************************************************************)

(************************* Helper functions & Globals ************************)
let indent_string str lvl =
  let lines = String.split_on_char '\n' str in
  let indentation = String.make lvl ' ' in
  String.concat "\n" (List.map (fun line -> indentation ^ line) lines)

let rec string_of_str_list (lst : 'a list) (elem_to_str : 'a -> string) (sep : string) =
  let buffer = Buffer.create 32 in
  let rec loop = function
    | [] -> ()
    | [elem] ->
      let elem_str = elem_to_str elem in
      if elem_str <> "" then
        Buffer.add_string buffer elem_str
    | elem :: rest ->
      let elem_str = elem_to_str elem in
      if elem_str <> "" then
        Buffer.add_string buffer (elem_str ^ sep);
      loop rest
  in loop lst;
  Buffer.contents buffer

let rec remove_duplicates lst =
  match lst with
  | [] -> []
  | hd :: tl ->
    if List.mem hd tl then
      remove_duplicates tl
    else
      hd :: remove_duplicates tl


(*************************** Ada Types and Variables *************************)
let rec string_of_ada_discrete_type = function
  | IntegerTyp -> "Integer"
  | NaturalTyp -> "Natural"
  | PositiveTyp -> "Positive"

and string_of_ada_real_type = function
  | FloatTyp -> "Float"

and string_of_ada_base_type = function
  | VoidTyp -> "null record"
  | Discrete s -> string_of_ada_discrete_type s
  | Real r -> string_of_ada_real_type r
  | StringTyp -> "String"

and string_of_ada_range = function
  | Closed (lb, ub) -> string_of_ada_expr lb ^ " .. " ^ string_of_ada_expr ub
  | Open (typ) -> string_of_ada_discrete_type typ ^ " range <>"

and string_of_ada_type = function
  | BaseType bt -> string_of_ada_base_type bt
  | Record (fields) ->
    let string_of_field (id_lst, typ) =
      "   " ^ (string_of_str_list id_lst (fun x -> x) ", ") ^ " : " ^
      string_of_ada_type_decl typ ^ ";"
    in
    "record\n" ^ (string_of_str_list fields string_of_field "\n") ^
    "\nend record"
  | Array (index_dims, ada_type, variant) -> (
      let buffer = Buffer.create 32 in
      let idx_lst_str = string_of_str_list index_dims string_of_ada_range ", " in
      begin match variant with
        | ElemTyp ->
          Buffer.add_string buffer "array (";
          Buffer.add_string buffer (idx_lst_str);
          Buffer.add_string buffer (") of " ^ (string_of_ada_type ada_type));
        | ArrTyp ->
          Buffer.add_string buffer ((string_of_ada_type ada_type) ^ " (");
          Buffer.add_string buffer (idx_lst_str);
          Buffer.add_string buffer (")");
      end;
      Buffer.contents buffer
    )
  | Access typ -> "access " ^ string_of_ada_type typ
  | NamedType str -> str

and string_of_ada_type_attr = function
  | In -> "in"
  | Out -> "out"
  | InOut -> "in out"
  | NotNull -> "not null"

and string_of_ada_type_attrs (attr_list : ada_type_attr list) =
  let buffer = Buffer.create 32 in
  List.iter (
    fun attr -> Buffer.add_string buffer ((string_of_ada_type_attr attr) ^ " ")
  ) attr_list;
  Buffer.contents buffer

and string_of_ada_type_decl (attr_list, ada_type) =
  let buffer = Buffer.create 32 in
  Buffer.add_string buffer (string_of_ada_type_attrs attr_list);
  Buffer.add_string buffer (string_of_ada_type ada_type);
  Buffer.contents buffer

and string_of_ada_var (name, type_decl) =
  name ^ " : " ^ string_of_ada_type_decl type_decl


(****************************** Ada expressions ******************************)
and string_of_ada_constant = function
  | Int i -> string_of_int i
  | Float f -> string_of_float f
  | String s -> s

and string_of_ada_unary_op = function
  | Minus -> "-"
  | Plus -> "+"
  | Not -> "not"

and string_of_ada_binary_op = function
  | Add -> "+"
  | Sub -> "-"
  | Mul -> "*"
  | Div -> "/"
  | Mod -> "mod"
  | And -> "and then"
  | Or  -> "or"
  | Eq  -> "="
  | Ne  -> "/="
  | Lt  -> "<"
  | Gt  -> ">"
  | Le  -> "<="
  | Ge  -> ">="
  | Assign -> ":="

and string_of_init_what = function
  | Nothing -> ""
  | InField (ident, (InField _ as in_f)) ->
    ident ^ "." ^ string_of_init_what in_f
  | InField (ident, Nothing) -> ident

and string_of_ada_expr = function
  | UnaryExpr (op, sub_e) ->
    string_of_ada_unary_op op ^ string_of_ada_expr sub_e
  | BinaryExpr (sub_e1, op, sub_e2) ->
    (string_of_ada_expr sub_e1) ^ " " ^ (string_of_ada_binary_op op) ^ " " ^
    (string_of_ada_expr sub_e2)
  | ParenExpr (sub_e) -> "(" ^ (string_of_ada_expr sub_e) ^ ")"
  | ConstantExpr (c) -> string_of_ada_constant c
  | VarExpr (v) -> v
  (* TODO: check if the ' attributes should not be MemberOfExpr *)
  | MemberOfExpr (sub_e, s) ->
    begin match sub_e with
      | ParenExpr _ | VarExpr _ | MemberOfExpr _ | TupleExpr _ ->
        string_of_ada_expr sub_e ^ (
          match s with
          | "last" -> "'Last"
          | "old" -> "'Old"
          | "range" -> "'Range"
          | _ -> "." ^ s
        )
      | _ ->
        "(" ^ string_of_ada_expr sub_e ^ ")" ^ (
          match s with
          | "last" -> "'Last"
          | "old" -> "'Old"
          | "range" -> "'Range"
          | _ -> "." ^ s
        )
    end
  | CastExpr (typ, expr) ->
    string_of_ada_type typ ^
    " (" ^ string_of_ada_expr expr ^ ")"
  | TupleExpr expr_list ->
    "(" ^ (string_of_str_list expr_list string_of_ada_expr ", ") ^ ")"
  | IndexExpr (expr, idx_list) ->
    begin match expr with
      | ParenExpr _ | VarExpr _ | MemberOfExpr _ | TupleExpr _ ->
        string_of_ada_expr expr ^
        " (" ^ (string_of_str_list idx_list string_of_ada_expr ", ") ^ ")"
      | _ ->
        "(" ^ string_of_ada_expr expr ^ ")" ^
        " (" ^ (string_of_str_list idx_list string_of_ada_expr ", ") ^ ")"
    end
  | CompoundExpr init_list -> "(" ^ (
      string_of_str_list init_list (fun (init_what, expr) -> match init_what with
        | Nothing -> string_of_ada_expr expr
        | InField _ -> (string_of_init_what init_what) ^ " => " ^
                       (string_of_ada_expr expr)) ", "
    ) ^ ")"
  | ProcedureCall (ident, params) ->
    ident ^ " (" ^
    string_of_str_list params string_of_ada_expr ", " ^ ")"
  | KernelCall (ident, (block_dim, thread_dim), params) ->
    "pragma Cuda_Execute (\n" ^
      indent_string ident 3 ^ " (" ^
        string_of_str_list params string_of_ada_expr ", " ^ "),\n" ^
      indent_string (string_of_ada_expr block_dim) 3 ^ ",\n" ^
      indent_string (string_of_ada_expr thread_dim) 3 ^ "\n" ^
    ")"
  | AllocExpr (typ_name, typ_args) ->
    "new " ^ typ_name ^ " (" ^
    (string_of_str_list typ_args string_of_ada_expr ", ") ^ ")"


(************************ Higher-level Ada Constructs ************************)
let rec string_of_ada_else ada_else lvl = match ada_else with
  | ElseStop -> indent_string "end if;" lvl ^ "\n"
  | Elsif (expr, then_blk, else_blk) ->
    indent_string "elsif " lvl ^ string_of_ada_expr expr ^ " then\n" ^
    string_of_ada_block then_blk (lvl + 3) ^
    string_of_ada_else else_blk lvl
  | Else blk ->
    indent_string "else\n" lvl ^
    string_of_ada_block blk (lvl + 3) ^
    indent_string "end if;" lvl ^ "\n"

and string_of_ada_stmnt stmnt lvl = match stmnt with
  | Computation (expr) -> indent_string (string_of_ada_expr expr) lvl ^ ";\n"
  | Block (blk) ->
    indent_string "begin" lvl ^ "\n" ^
    string_of_ada_block blk (lvl + 3) ^
    indent_string "end;" lvl ^ "\n"
  | If (expr, then_blk, else_blk) ->
    indent_string "if " lvl ^ string_of_ada_expr expr ^ " then\n" ^
    string_of_ada_block then_blk (lvl + 3) ^
    string_of_ada_else else_blk lvl
  | Return expr ->
    (indent_string "return " lvl) ^ string_of_ada_expr expr ^ ";\n"
  | Definition def -> string_of_ada_def def lvl
  | Pragma sp -> indent_string (string_of_spark_pragma sp) lvl ^ "\n"

and string_of_ada_block blk lvl =
  let buffer = Buffer.create 32 in
  let rec loop = function
    | [] -> ()
    | stmnt :: rest ->
        Buffer.add_string buffer (string_of_ada_stmnt stmnt lvl);
        loop rest
  in loop blk;
  Buffer.contents buffer

and string_of_ada_decl_block blk lvl =
  let buffer = Buffer.create 32 in
  let rec loop = function
    | [] -> ()
    | def :: rest ->
        Buffer.add_string buffer (string_of_ada_def def lvl);
        loop rest
  in loop blk;
  Buffer.contents buffer

and string_of_ada_typedef_attr attr = match attr with
  | CudaStorageModel -> "Designated_Storage_Model => CUDA.Storage_Models.Model"

and string_of_ada_typedef type_def = match type_def with
  | FullType (name, typ, attrs) ->
    if attrs <> [] then
      "type " ^ name ^ " is " ^ string_of_ada_type typ ^ " with\n" ^
      indent_string (string_of_str_list attrs string_of_ada_typedef_attr ", ") 2
    else
      "type " ^ name ^ " is " ^ string_of_ada_type typ
  | SubType (name, typ, Some (lb), Some (ub), attrs) ->
    if attrs <> [] then
      "type " ^ name ^ " is new " ^ string_of_ada_type typ ^ " range " ^
      string_of_ada_expr lb ^ " .. " ^ string_of_ada_expr ub ^ " with\n" ^
      indent_string (string_of_str_list attrs string_of_ada_typedef_attr ", ") 2
    else
      "type " ^ name ^ " is new " ^ string_of_ada_type typ ^ " range " ^
      string_of_ada_expr lb ^ " .. " ^ string_of_ada_expr ub
  | SubType (name, typ, Some (lb), None, attrs) ->
    if attrs <> [] then
      "type " ^ name ^ " is new " ^ string_of_ada_type typ ^ " range " ^
      string_of_ada_expr lb ^ " .. " ^ " with\n" ^
      indent_string (string_of_str_list attrs string_of_ada_typedef_attr ", ") 2
    else
      "type " ^ name ^ " is new " ^ string_of_ada_type typ ^ " range " ^
      string_of_ada_expr lb ^ " .. "
  | SubType (name, typ, None, Some (ub), attrs) ->
    if attrs <> [] then
      "type " ^ name ^ " is new " ^ string_of_ada_type typ ^ " range " ^
      " .. " ^ string_of_ada_expr ub ^ " with\n" ^
      indent_string (string_of_str_list attrs string_of_ada_typedef_attr ", ") 2
    else
      "type " ^ name ^ " is new " ^ string_of_ada_type typ ^ " range " ^
      " .. " ^ string_of_ada_expr ub
  | _ -> raise (InternalTranspilerError "Invalid range directive")

and string_of_ada_func_attr func_attr = match func_attr with
  | PreAttr expr -> "Pre => " ^ string_of_ada_expr expr
  | PostAttr expr -> "Post => " ^ string_of_ada_expr expr
  | Target target -> begin
      match target with
      | Global -> "CUDA_Global"
      | Device -> "CUDA_Device"
      | Host -> ""
    end
  | SparkMode sm -> begin
      match sm with
      | Default -> ""
      | On -> "SPARK_Mode => On"
      | Off -> "SPARK_Mode => Off"
    end

and string_of_ada_def def lvl = match def with
  | FunDef (_, name, attrs, args, decl_blk, body) ->
    let proto =
      "\nprocedure " ^ name ^ (
        if args <> [] then
          " (\n" ^
          indent_string (string_of_str_list args string_of_ada_var ";\n") 2 ^ "\n) "
        else
          "\n"
      ) ^
      let body_attrs = List.filter (fun attr -> match attr with
        | SparkMode _ -> true
        | _ -> false
      ) attrs in
      if body_attrs <> [] then
        "with\n" ^
        indent_string (
          string_of_str_list body_attrs string_of_ada_func_attr ",\n"
        ) 2 ^ "\n"
      else if args <> [] then
        "\n"
      else
        ""
    in
    let body =
      "is\n" ^
        string_of_ada_decl_block decl_blk 3 ^
      "begin\n\n" ^
        string_of_ada_block body 3 ^
      "\nend " ^ name ^ ";\n" in
    indent_string (proto ^ body) lvl
  | FunDecl (_, name, attrs, args) ->
    indent_string "\nprocedure " lvl ^ name ^ (
        if args <> [] then
          " (\n" ^
          indent_string (string_of_str_list args string_of_ada_var ";\n") (lvl + 2) ^
          indent_string "\n)" lvl
        else
          ""
      ) ^
      if attrs = [] then
        ";\n\n"
      else
        " with\n" ^
        indent_string (
          string_of_str_list attrs string_of_ada_func_attr ",\n"
        ) (lvl + 2) ^ ";\n\n"
  | VarDef (idents, type_decl, expr) ->
    indent_string (string_of_str_list idents (fun x -> x) ", ") lvl ^ " : " ^
    string_of_ada_type_decl type_decl ^ " := " ^
    string_of_ada_expr expr ^ ";\n"
  | VarDecl (idents, type_decl) ->
    indent_string (string_of_str_list idents (fun x -> x) ", ") lvl ^ " : " ^
    string_of_ada_type_decl type_decl ^ ";\n"
  | TypeDef td ->
    indent_string (string_of_ada_typedef td) lvl ^ ";\n"
  | PragmaDef sp -> indent_string (string_of_spark_pragma sp) lvl ^ "\n"

(* TODO *)
and string_of_ada_package pkg =
  ""


(******************************** Ada Pragmas ********************************)
and string_of_spark_pragma sp =
  begin match sp with
    | Assume expr ->
      let rec find_size_directive_vars = function
        | MemberOfExpr (VarExpr (ident), "last") -> [ident]
        | BinaryExpr (expr1, _, expr2) ->
            find_size_directive_vars expr1 @ find_size_directive_vars expr2
        | UnaryExpr (_, expr) -> find_size_directive_vars expr
        | ParenExpr (expr) -> find_size_directive_vars expr
        | _ -> []
      in
      let vars = remove_duplicates (find_size_directive_vars expr) in
      let buffer = Buffer.create 32 in
      Buffer.add_string buffer "pragma Assume (";
      let rec loop = function
        | [] -> ()
        | [var] -> Buffer.add_string buffer (var ^ "'First = 0);")
        | var :: rest -> Buffer.add_string buffer (var ^ "'First = 0 and ");
          loop rest
      in loop vars;
      if (List.length vars) > 0 then
        "pragma Assume (" ^ string_of_ada_expr expr ^ ");\n" ^
        Buffer.contents buffer
      else
        "pragma Assume (" ^ string_of_ada_expr expr ^ ");"
    | Assert expr ->
      let rec find_size_directive_vars = function
        | MemberOfExpr (VarExpr (ident), "last") -> [ident]
        | BinaryExpr (expr1, _, expr2) ->
            find_size_directive_vars expr1 @ find_size_directive_vars expr2
        | UnaryExpr (_, expr) -> find_size_directive_vars expr
        | ParenExpr (expr) -> find_size_directive_vars expr
        | _ -> []
      in
      let vars = remove_duplicates (find_size_directive_vars expr) in
      let buffer = Buffer.create 32 in
      Buffer.add_string buffer "pragma Assert (";
      let rec loop = function
        | [] -> ()
        | [var] -> Buffer.add_string buffer (var ^ "'First = 0);\n")
        | var :: rest -> Buffer.add_string buffer (var ^ "'First = 0 and ");
          loop rest
      in loop vars;
      if (List.length vars) > 0 then
        "pragma Assert (" ^ string_of_ada_expr expr ^ ");\n" ^
        Buffer.contents buffer
      else
        "pragma Assert (" ^ string_of_ada_expr expr ^ ");"
    | _ -> ""  (* Silent pragmas *)
  end
