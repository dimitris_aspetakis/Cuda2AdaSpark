open Adaabs

type 'a visitAction =
    SkipChildren                        (* Do not visit the children. Return
                                         * the node as it is *)
  | ChangeTo of 'a                      (* Replace the expression with the
                                         * given one *)
  | DoChildren                          (* Continue with the children of this
                                         * node. Rebuild the node on return
                                         * if any of the children changes
                                         * (use == test) *)
  | ChangeDoChildrenPost of 'a * ('a -> 'a) (* First consider that the entire
                                          * exp is replaced by the first
                                          * paramenter. Then continue with
                                          * the children. On return rebuild
                                          * the node if any of the children
                                          * has changed and then apply the
                                          * function on the node *)

class type adaabsVisitor = object
  method vtype: ada_type -> ada_type visitAction
  method vexpr: ada_expr -> ada_expr visitAction
  method vstmnt: ada_stmnt -> ada_stmnt list visitAction
  method vblock: ada_block -> ada_block visitAction
  method vdef: ada_definition -> ada_definition list visitAction
  method vdeclblock: ada_decl_block -> ada_decl_block visitAction
end

class nopAdaabsVisitor: adaabsVisitor

val visitAdaabsType: adaabsVisitor -> ada_type -> ada_type
val visitAdaabsExpr: adaabsVisitor -> ada_expr -> ada_expr
val visitAdaabsStmnt: adaabsVisitor -> ada_stmnt -> ada_stmnt list
val visitAdaabsBlock: adaabsVisitor -> ada_block -> ada_block
val visitAdaabsDefinition: adaabsVisitor -> ada_definition -> ada_definition list
val visitAdaabsDeclBlock: adaabsVisitor -> ada_decl_block -> ada_decl_block
val visitAdaabsDefList: adaabsVisitor -> ada_definition list -> ada_definition list
