open Adaabs
open Cabs

val ada_typedefs : ada_typedef list ref

val transpile_specifier : Cabs.specifier -> ada_type_decl

val transpile_decl_type : Cabs.specifier -> Cabs.decl_type -> ada_type_decl

val transpile_expr : Cabs.expression -> ada_expr

val transpile_stmnt : Cabs.statement -> ada_stmnt list

val transpile_else : Cabs.statement -> ada_else

val transpile_block : Cabs.block -> ada_block

val transpile_single_name : Cabs.single_name -> ada_var

val transpile_proto : Cabs.single_name -> spark_pragma list -> ada_target * (ada_ident * ada_var list)

val transpile_def : Cabs.definition -> ada_definition list

val group_fundef_pragmas : Cabs.definition list -> Cabs.definition list
