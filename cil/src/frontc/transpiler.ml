open Adaabs
open Cabs

(************ Global variables & helpers needed for transpilation ************)
let cabslu = {
  lineno = -10; 
	filename = "cabs loc unknown"; 
	byteno = -10;
  ident = 0
}

let ada_typedefs : ada_typedef list ref = ref []

let add_typedef (td : ada_typedef) : unit =
  if not (List.mem td !ada_typedefs) then
    ada_typedefs := !ada_typedefs @ [td]
  else ()

let filter_map f lst =
  let rec aux acc = function
    | [] -> List.rev acc
    | x :: xs ->
      match f x with
      | Some y -> aux (y :: acc) xs
      | None -> aux acc xs
  in
  aux [] lst

let group_fundef_pragmas (def_list) : definition list =
  let rec aux (acc : spark_pragma list) = function
    | [] -> List.rev (List.map (fun sp -> SPARK_PRAGMA_DEF (sp, cabslu)) acc)
    | h :: t -> match h with
      | FUNDEF (sn, b, sp, l1, l2) ->
        let pragma_group = List.rev acc in
        FUNDEF (sn, b, Some(pragma_group), l1, l2) :: aux [] t
      | SPARK_PRAGMA_DEF (sp, _) -> aux (sp :: acc) t
      | _ as other -> other :: aux acc t
  in
  aux [] def_list

let rec remove_duplicates lst =
  match lst with
  | [] -> []
  | hd :: tl ->
    if List.mem hd tl then
      remove_duplicates tl
    else
      hd :: remove_duplicates tl

let rec count_nested_ptrs (dt : decl_type) : int =
  match dt with
  | JUSTBASE -> 0
  | PARENTYPE (_, inner_dt, _)
  | ARRAY (inner_dt, _, _)
  | PROTO (inner_dt, _, _) ->
    count_nested_ptrs inner_dt
  | PTR (_, inner_dt) -> 1 + count_nested_ptrs inner_dt

and construct_ada_array_type (dims : int) (typ : ada_type) : ada_type =
  let rec create_list element n =
  if n <= 0 then
    []
  else
    Open (element) :: create_list element (n - 1)
  in
  Adaabs.Array ((create_list Adaabs.NaturalTyp dims), typ, ElemTyp)

and gen_range_scalar_type (range : spark_pragma) (elem_type : ada_type) =
  match range with
  | ScalarRange (_, Some(lb_expr), Some(ub_expr)) ->
    let (lb, ub) = match (lb_expr, ub_expr) with
      | ConstantExpr (Int (lb)), ConstantExpr (Int (ub)) -> lb, ub
      | UnaryExpr (Minus, ConstantExpr (Int (lb))),
        ConstantExpr (Int (ub)) -> -lb, ub
      | ConstantExpr (Int (lb)),
        UnaryExpr (Minus, ConstantExpr (Int (ub))) -> lb, -ub
      | UnaryExpr (Minus, ConstantExpr (Int (lb))),
        UnaryExpr (Minus, ConstantExpr (Int (ub))) -> -lb, -ub
      | _ -> raise (InternalTranspilerError "ScalarRange expects integer values for ranges")
    in
    let elem_typ_name = string_of_ada_type elem_type ^ "_" ^
      (if lb >= 0 then string_of_int lb else "neg" ^ string_of_int (abs lb)) ^ "_" ^
      (if ub >= 0 then string_of_int ub else "neg" ^ string_of_int (abs ub))
    in
    let elem_typ = SubType (elem_typ_name, elem_type, Some(lb_expr), Some(ub_expr), []) in
    add_typedef elem_typ;
    NamedType(elem_typ_name)
  | ScalarRange (_, None, Some(ub_expr)) ->
    let ub = match ub_expr with
      | ConstantExpr (Int (ub)) -> ub
      | UnaryExpr (Minus, ConstantExpr (Int (ub))) -> -ub
      | _ -> raise (InternalTranspilerError "ScalarRange expects integer values for ranges")
    in
    let elem_typ_name = string_of_ada_type elem_type ^
      "_open_" ^ (if ub >= 0 then string_of_int ub else "neg" ^ string_of_int (abs ub)) in
    let elem_typ = SubType (elem_typ_name, elem_type, None, Some(ub_expr), []) in
    add_typedef elem_typ;
    NamedType(elem_typ_name)
  | ScalarRange (_, Some(lb_expr), None) ->
    let lb = match lb_expr with
      | ConstantExpr (Int (lb)) -> lb
      | UnaryExpr (Minus, ConstantExpr (Int (lb))) -> -lb
      | _ -> raise (InternalTranspilerError "ScalarRange expects integer values for ranges")
    in
    let elem_typ_name = string_of_ada_type elem_type ^ "_" ^
      (if lb >= 0 then string_of_int lb else "neg" ^ string_of_int (abs lb)) ^ "_open" in
    let elem_typ = SubType (elem_typ_name, elem_type, Some(lb_expr), None, []) in
    add_typedef elem_typ;
    NamedType(elem_typ_name)
  | _ -> raise (InternalTranspilerError "ScalarRange expected")

and gen_arg_arr_type (sn : single_name) (sp_list : spark_pragma list)
                     (target : ada_target) =
  match sn with
  | [SpecType(spec)], (name, (Cabs.PTR (_, _) as decl_type), _, _) -> (
      let dim_depth = count_nested_ptrs decl_type in
      let elem_typ = transpile_type_spec spec in
      let pragma = try Some(
        List.find (fun sp -> match sp with
          | ScalarRange (pragma_var_name, _, _) -> pragma_var_name = name
          | _ -> false
        ) sp_list
      ) with Not_found -> None
      in match pragma with
      | Some (ScalarRange _ as sr) ->
        let elem_typ = gen_range_scalar_type sr elem_typ in
        let arr_typ_name = string_of_ada_type elem_typ ^ "_Array_" ^
          string_of_int dim_depth
        in
        add_typedef (
          FullType (arr_typ_name, construct_ada_array_type dim_depth elem_typ, [])
        );
        if target <> Host then begin
          let arr_access_typ_name = arr_typ_name ^ "_Device_Access" in
          FullType (arr_access_typ_name, Access (NamedType (arr_typ_name)), [CudaStorageModel])
        end else begin
          FullType (arr_typ_name, NamedType (arr_typ_name), [])
        end
      | None ->
        let arr_typ_name = string_of_ada_type elem_typ ^ "_Array_" ^
          string_of_int dim_depth
        in
        add_typedef (
          FullType (arr_typ_name, construct_ada_array_type dim_depth elem_typ, [])
        );
        if target <> Host then begin
          let arr_access_typ_name = arr_typ_name ^ "_Device_Access" in
          FullType (arr_access_typ_name, Access (NamedType (arr_typ_name)), [CudaStorageModel])
        end else begin
          FullType (arr_typ_name, NamedType (arr_typ_name), [])
        end
      | _ -> raise (InternalTranspilerError "Should be unreachable")
    )
  | _ -> raise (InternalTranspilerError "Can't convert type to an Ada array")

and transpile_type_spec (typ : Cabs.typeSpecifier) : ada_type = match typ with
  | Tvoid -> Adaabs.BaseType (Adaabs.VoidTyp)
  | Tint | Tlong | Tint64 | Tsigned ->
    Adaabs.BaseType (Adaabs.Discrete (Adaabs.IntegerTyp))
  | Tfloat | Tdouble -> Adaabs.BaseType (Adaabs.Real (Adaabs.FloatTyp))
  | Tunsigned -> Adaabs.BaseType (Adaabs.Discrete (Adaabs.NaturalTyp))
  | Tnamed s -> Adaabs.NamedType s
  (* TODO: add support for enums and structs *)
  | _ -> raise (InternalTranspilerError "Unsupported C type")

and spark_pragma_to_func_attr (sp : Adaabs.spark_pragma) : ada_func_attr =
  match sp with
  | Pre exp -> PreAttr exp
  | Post exp -> PostAttr exp
  | _ -> raise (InternalTranspilerError "Expected function attribute")

and get_ada_target (specs : Cabs.specifier) : ada_target =
  match specs with
  | SpecGlobalKernel :: _ -> Global
  | SpecDeviceKernel :: _ -> Device
  | h :: t -> get_ada_target t 
  | [] -> Host

and transform_decls (target : Adaabs.ada_target) (decls : Adaabs.ada_decl_block)
  (var_attrs : spark_pragma list) : Adaabs.ada_decl_block =
  filter_map (fun d ->
    match d with
    | VarDef (id_lst, (attrs, Array (range_lst, typ, elem_typ)), expr) ->
      let pragma = try Some(
        List.find (fun sp -> match sp with
          | ScalarRange (pragma_var_name, _, _) ->
            List.mem pragma_var_name id_lst
          | _ -> false
        ) var_attrs
      ) with Not_found -> None in
      begin match pragma with
        | Some sr ->
          let elem_typ = gen_range_scalar_type sr typ in
          let dim_depth = List.length range_lst in
          let arr_typ_name = string_of_ada_type elem_typ ^ "_Array_" ^
            string_of_int dim_depth in
          let arr_typ = construct_ada_array_type dim_depth elem_typ in
          add_typedef (FullType (arr_typ_name, arr_typ, []));
          Some (
            VarDef (id_lst, (attrs,
              Adaabs.Array (range_lst, NamedType (arr_typ_name), ArrTyp)
            ), expr)
          )
        | _ -> Some (d)
      end
    | VarDecl (id_lst, (attrs, Array (range_lst, typ, elem_typ))) ->
      let pragma = try Some(
        List.find (fun sp -> match sp with
          | ScalarRange (pragma_var_name, _, _) ->
            List.mem pragma_var_name id_lst
          | _ -> false
        ) var_attrs
      ) with Not_found -> None in
      begin match pragma with
        | Some sr ->
          let elem_typ = gen_range_scalar_type sr typ in
          let dim_depth = List.length range_lst in
          let arr_typ_name = string_of_ada_type elem_typ ^ "_Array_" ^
            string_of_int dim_depth in
          let arr_typ = construct_ada_array_type dim_depth elem_typ in
          add_typedef (FullType (arr_typ_name, arr_typ, []));
          Some (
            VarDecl (id_lst, (attrs,
              Adaabs.Array (range_lst, NamedType (arr_typ_name), ArrTyp)
            ))
          )
        | _ -> Some (d)
      end
    | _ -> Some (d)
  ) decls
(*****************************************************************************)


(**************************** Transpiler Functions ***************************)
let rec transpile_specifier (specs : Cabs.specifier) : ada_type_decl =
  match specs with
  | SpecCV (CV_CONST) :: rest ->
    let (attrs, typ) = transpile_specifier rest in
    (Adaabs.In :: attrs, typ)
  | SpecType ts :: _ -> ([], transpile_type_spec ts)
  | SpecGlobalKernel :: rest -> transpile_specifier rest
  | _ -> raise (InternalTranspilerError "Unsupported type specifier")

(* Not for function prototypes *)
and transpile_decl_type (spec : Cabs.specifier) (dt : Cabs.decl_type) : ada_type_decl =
  let (attrs, typ) = transpile_specifier spec in
  (* TODO: rework for multi-dimensional arrays when Adaabs.Array becomes recursive *)
  let rec constr_array_attrs typ dt = match dt with
  | ARRAY (_, _, CONSTANT (CONST_INT (int_str))) ->
    Adaabs.Array (
      [ Adaabs.Closed (
          Adaabs.ConstantExpr (Adaabs.Int (0)),
          Adaabs.ConstantExpr (Adaabs.Int (int_of_string int_str - 1))
        )
      ], typ, ElemTyp
    )
  | ARRAY (_, _, NOTHING) ->
    Adaabs.Array ([Adaabs.Open (Adaabs.NaturalTyp)], typ, ElemTyp)
  | JUSTBASE -> typ
  | PTR (_, dt') ->
    let (_, typ) = transpile_decl_type spec dt' in
    Adaabs.Access typ
  | _ -> raise (InternalTranspilerError "Unsupported type declaration")
  in attrs, (constr_array_attrs typ dt)

and transpile_expr (e : Cabs.expression) : ada_expr = match e with
  | UNARY (op, sub_e) ->
    Adaabs.UnaryExpr (
      begin match op with
        | MINUS -> Adaabs.Minus
        | PLUS ->  Adaabs.Plus
        | NOT ->  Adaabs.Not
        | _ -> raise (InternalTranspilerError "Unsupported unary expression")
      end,
      (transpile_expr sub_e)
    )
  | BINARY (op, sub_e1, sub_e2) ->
    Adaabs.BinaryExpr (
      (transpile_expr sub_e1),
      begin match op with
        | ADD -> Adaabs.Add
        | SUB -> Adaabs.Sub
        | MUL -> Adaabs.Mul
        | DIV -> Adaabs.Div
        | MOD -> Adaabs.Mod
        | AND -> Adaabs.And
        | OR -> Adaabs.Or
        | EQ -> Adaabs.Eq
        | NE -> Adaabs.Ne
        | LT -> Adaabs.Lt
        | GT -> Adaabs.Gt
        | LE -> Adaabs.Le
        | GE -> Adaabs.Ge
        | ASSIGN -> Adaabs.Assign
        | _ -> raise (InternalTranspilerError "Unsupported binary expression")
      end,
      (transpile_expr sub_e2)
    )
  | PAREN sub_e -> Adaabs.ParenExpr (transpile_expr sub_e)
  (* TODO: compound inits someday?? *)
  | CAST ((spec, dt), SINGLE_INIT (expr)) ->
    let (_, typ) = transpile_decl_type spec dt in
    Adaabs.CastExpr (typ, transpile_expr expr)
  | CONSTANT const ->
    Adaabs.ConstantExpr (
      match const with
        | CONST_INT c -> Adaabs.Int (int_of_string c)
        | CONST_FLOAT c -> Adaabs.Float (float_of_string c)
        | CONST_STRING c -> Adaabs.String (c)
        | _ -> raise (InternalTranspilerError "Unsupported constant")
    )
  | VARIABLE v -> Adaabs.VarExpr (v)
  (* TODO: handle multi-dimensional arrays *)
  | INDEX (expr, idx) ->
    Adaabs.IndexExpr (transpile_expr expr, [transpile_expr idx])
  | MEMBEROF (sub_e, s) ->
    Adaabs.MemberOfExpr (transpile_expr sub_e, s)
  | CALL (VARIABLE (ident), params) ->
    let params = List.map transpile_expr params in
    ProcedureCall (ident, params)
  | CUDAKERNELCALL (VARIABLE (ident), dims, params) ->
    let params = List.map transpile_expr params in
    let dims = match dims with
      | VARIABLE bv :: VARIABLE tv :: _ -> (VarExpr bv, VarExpr tv)
      | _ -> raise (InternalTranspilerError "Unsupported kernel call convention")
    in
    KernelCall (ident, dims, params)
  | _ -> raise (InternalTranspilerError "Unsupported expression")

and transpile_stmnt (s : Cabs.statement) : ada_stmnt list = match s with
  | NOP _ -> []
  | COMPUTATION (CALL (VARIABLE (ident), params), _) when
    ident = "cudaMalloc" || ident = "cudaFree" ->
      []
  | COMPUTATION (CALL (
      VARIABLE (ident),
      [_; VARIABLE var; CONSTANT (CONST_INT (int_str)); VARIABLE direction]
    ), _) when ident = "cudaMemcpy" && direction = "cudaMemcpyHostToDevice" ->
    [Adaabs.Pragma (Assert (
      BinaryExpr (MemberOfExpr (VarExpr var, "last"), Eq, ConstantExpr (Int (
        (int_of_string int_str) - 1
      )))
    ))]
  | COMPUTATION (CALL (
      VARIABLE (ident),
      [VARIABLE var; _; CONSTANT (CONST_INT (int_str)); VARIABLE direction]
    ), _) when ident = "cudaMemcpy" && direction = "cudaMemcpyDeviceToHost" ->
    [Adaabs.Pragma (Assert (
      BinaryExpr (MemberOfExpr (VarExpr var, "last"), Eq, ConstantExpr (Int (
        (int_of_string int_str) - 1
      )))
    ))]
  | COMPUTATION (expr, _) -> [Adaabs.Computation (transpile_expr expr)]
  | BLOCK (blk, _) -> transpile_block blk
  | SEQUENCE (stmnt1, stmnt2, _) ->
    transpile_stmnt stmnt1 @ transpile_stmnt stmnt2
  | IF (expr, then_stmnt, else_stmnt, _) -> [
      Adaabs.If (transpile_expr expr, transpile_stmnt then_stmnt,
                 transpile_else else_stmnt)
    ]
  | RETURN (expr, _) -> [Adaabs.Return (transpile_expr expr)]
  | DEFINITION def ->
    List.map (fun d -> Adaabs.Definition d) (transpile_def def)
  | SPARK_PRAGMA (sp, _) -> [Adaabs.Pragma (sp)]
  | _ -> raise (InternalTranspilerError "Unsupported statement")

and transpile_else (s : Cabs.statement) : ada_else = match s with
  | NOP _ -> Adaabs.ElseStop
  | COMPUTATION _ | BLOCK _ | RETURN _ | DEFINITION _ | SEQUENCE (_, NOP _, _) ->
    Adaabs.Else (transpile_stmnt s)
  | IF (expr, stmnt1, stmnt2, _) -> Adaabs.Elsif (
      transpile_expr expr,
      transpile_stmnt stmnt1,
      transpile_else stmnt2
    )
  | _ -> raise (InternalTranspilerError "Unsupported else statement")

and transpile_block (b : Cabs.block) : ada_block =
  let stmnts : ada_stmnt list ref = ref [] in
  List.iter (fun s -> stmnts := !stmnts @ transpile_stmnt s) b.bstmts;
  !stmnts

and transpile_single_name (sn : Cabs.single_name) : ada_var = match sn with
  | (spec, (name, decl, _, _)) -> (name, transpile_decl_type spec decl)

and transpile_proto (sn : Cabs.single_name) (arg_attrs : spark_pragma list) :
  ada_target * (ada_ident * ada_var list) = match sn with
  | (spec, (fname, PROTO (dt, args, _), _, _)) ->
    let target = get_ada_target spec in

    (* Generate array type declarations given the spark_pragma directives *)
    (* TODO: also catch args with array notation here, e.g. a[] *)
    let array_args, scalar_args = List.partition (fun a ->
      match a with
        | _, (_, Cabs.PTR (_, _), _, _) -> true
        | _ -> false
    ) args in

    let ada_arr_args = List.map (fun ((_, (var_name, _, _, _)) as c_arg) -> (
        let ada_arg_type = gen_arg_arr_type c_arg arg_attrs target in
        add_typedef ada_arg_type;
        match ada_arg_type with
          | FullType (type_name, _, _) ->
            if target <> Host then
              (var_name, ([Adaabs.NotNull], Adaabs.NamedType type_name))
            else
              (var_name, ([], Adaabs.NamedType type_name))
          | _ ->
            raise (InternalTranspilerError "Only FullType expected here")
      )
    ) array_args
    in

    (* Create the scalar arguments *)
    (* TODO: Take into account any applicable spark declarations *)
    let ada_sc_args = List.map transpile_single_name scalar_args in

    let ada_args =
      List.map (fun ((ident, (attrs, typ)) : ada_var) ->
        (* TODO: add in, out and in/out attributes to arguments according to *)
        let (in_args, out_args, inout_args) = List.fold_left (
          fun (in_lst, out_lst, inout_lst) elem ->
            match elem with
            | InArgs lst ->
              (in_lst @ lst, out_lst, inout_lst)
            | OutArgs lst ->
              (in_lst, out_lst @ lst, inout_lst)
            | InOutArgs lst ->
              (in_lst, out_lst, inout_lst @ lst)
            | _ ->
              (in_lst, out_lst, inout_lst)
        ) ([], [], []) arg_attrs in
        if List.mem ident inout_args then
          (ident, (Adaabs.InOut :: attrs, typ))
        else if List.mem ident out_args then
          (ident, (Adaabs.Out :: attrs, typ))
        else if List.mem ident in_args then
          (ident, (Adaabs.In :: attrs, typ))
        else
          (ident, (attrs, typ))
      ) (ada_arr_args @ ada_sc_args)
    in
    
    let (attrs, ret_arg) = transpile_decl_type spec dt in
    if ret_arg = BaseType (VoidTyp) || fname = "main" then
      if fname = "main" then
        target, ("Main", [])
      else
        target, (fname, ada_args)
    else
      target,
      (
        fname, ada_args @ [
          ("return_arg", (Adaabs.Out :: attrs, ret_arg))
        ]
      )
  | _ -> raise (InternalTranspilerError "Unsupported function prototype")

(* TODO: add the missing definitions *)
and transpile_def (d : Cabs.definition) : ada_definition list = match d with
  | FUNDEF (proto, body, spark_pragmas, _, _) ->
    (* Separate spark pragmas to their Ada variants *)
    let (decl_pragmas, var_attrs, arg_attrs, func_attrs) = match spark_pragmas with
      | Some (sp) ->
        List.fold_left (fun (dp_lst, va_lst, aa_lst, fa_lst) elem ->
          match elem with
          | ScalarRange _ ->
            (dp_lst, va_lst @ [elem], aa_lst @ [elem], fa_lst)
          | InArgs _ | OutArgs _ | InOutArgs _ ->
            (dp_lst, va_lst, aa_lst @ [elem], fa_lst)
          | Pre _ ->
            (dp_lst @ [elem], va_lst, aa_lst, fa_lst @ [elem])
          | Post _ ->
            (dp_lst, va_lst, aa_lst, fa_lst @ [elem])
          (* TODO: probably add a warning here for unsupported pragmas in global scope *)
          | Assert _ | Assume _ ->
            (dp_lst, va_lst, aa_lst, fa_lst)
        ) ([], [], [], []) sp
      | None -> ([], [], [], [])
    in

    (* Transpile the prototype of the function *)
    let target, (fname, args) = transpile_proto proto arg_attrs in

    let decl_pragmas =
      if target <> Host then
        decl_pragmas
      else
        []
    in
    

    let is_def (st : Cabs.statement) = match st with
      | DEFINITION _ -> true
      | _ -> false
    in
    let is_pragma (st : Cabs.statement) = match st with
      | SPARK_PRAGMA _ -> true
      | _ -> false
    in
    (* TODO: move this to a visitor pass to catch everything recursively *)
    let is_not_silent_pragma_def (d : ada_definition) = match d with
      | PragmaDef sp ->
        begin match sp with
          | Pre _ | Post _ | ScalarRange _
          | InArgs _ | OutArgs _ | InOutArgs _ -> false
          | _ -> true
        end
      | _ -> true
    in
    (* TODO: move this to a visitor pass to catch everything recursively *)
    let is_not_silent_pragma_stmnt (st : ada_stmnt) = match st with
      | Pragma sp ->
        begin match sp with
          | Pre _ | Post _ | ScalarRange _
          | InArgs _ | OutArgs _ | InOutArgs _ -> false
          | _ -> true
        end
      | _ -> true
    in
    (* Separate declarations from statements *)
    let separate_defs (c_stmnts : Cabs.statement list) :
      Cabs.statement list * Cabs.statement list =
      let last_is_def : bool ref = ref (is_def (List.hd c_stmnts)) in
      let c_decls : Cabs.statement list ref = ref [] in
      let c_body : Cabs.statement list ref = ref [] in
      List.iter (fun s -> match s with
        | s when is_pragma s ->
          if !last_is_def then
            c_decls := !c_decls @ [s]
          else
            c_body := !c_body @ [s]
        | s when is_def s ->
          c_decls := !c_decls @ [s];
          last_is_def := true;
        | s ->
          c_body := !c_body @ [s];
          last_is_def := false;
      ) c_stmnts;
      (!c_decls, !c_body)
    in
    let (c_decls, c_body) = separate_defs body.bstmts in
    let (decls, body) = (
      List.flatten (List.map (fun st -> match st with
        | DEFINITION d -> transpile_def d
        | SPARK_PRAGMA (d, _) -> [Adaabs.PragmaDef d]
        | _ -> raise (InternalTranspilerError "Expected definition")
      ) c_decls) |>
      (* TODO: move this to a visitor pass to catch everything recursively *)
      List.filter is_not_silent_pragma_def,
      List.flatten (List.map transpile_stmnt c_body) |>
      (* TODO: move this to a visitor pass to catch everything recursively *)
      List.filter is_not_silent_pragma_stmnt
    )
    in
    let func_attrs = 
      if target <> Host then
        (List.map spark_pragma_to_func_attr func_attrs) @ [Target (target)]
      else
        List.map spark_pragma_to_func_attr func_attrs
    in
    let decls' = transform_decls target decls var_attrs in
    [
      Adaabs.FunDef (
        target,
        fname,
        func_attrs,
        args,
        List.map (fun sp -> Adaabs.PragmaDef (sp)) decl_pragmas @ decls',
        body
      )
    ]
  | DECDEF ((spec, init_var_list), _) ->
    (* Create a list of type Ada variable definitions/declarations *)
    List.map (fun ((name, decl, _, _), expr) -> match expr with
      | SINGLE_INIT expr ->
        let (_, typ) as decl_typ = transpile_decl_type spec decl in
        begin match typ with
        | BaseType _ | NamedType _ ->
          Adaabs.VarDef ([name], decl_typ, Adaabs.CastExpr (typ, transpile_expr expr))
        | _ ->
          Adaabs.VarDef ([name], decl_typ, transpile_expr expr)
        end
      | NO_INIT ->
        Adaabs.VarDecl ([name], transpile_decl_type spec decl)
      | COMPOUND_INIT (init_list) ->
        let rec transpile_initwhat (i : Cabs.initwhat) : Adaabs.init_what = match i with
          | NEXT_INIT -> Nothing
          | INFIELD_INIT (field, initwhat) ->
            InField (field, transpile_initwhat initwhat)
          | _ -> raise (InternalTranspilerError "Unsupported compound init")
        in
        let rec transpile_init_expr (ie) : Adaabs.ada_expr = match ie with
          | NO_INIT ->
            raise (InternalTranspilerError "Unsupported compound init")
          | SINGLE_INIT e -> transpile_expr e
          | COMPOUND_INIT il ->
            let il' = List.map (fun (iw, ie) ->
              transpile_initwhat iw, transpile_init_expr ie
            ) il in
            CompoundExpr (il')
        in
        let compound_expr = CompoundExpr (
          List.map (fun (iw, ie) ->
            transpile_initwhat iw, transpile_init_expr ie
          ) init_list
        ) in
        Adaabs.VarDef ([name], transpile_decl_type spec decl, compound_expr)
      | _ -> raise (InternalTranspilerError "Unsupported global declaration")
    ) init_var_list
  | TYPEDEF ((spec, var_list), _) ->
    (* TODO: group declarations with the same type *)
    (* Create a list of type Ada variable declarations *)
    List.map (fun ((name, decl, _, _)) ->
      let decl_typ = transpile_decl_type spec decl in
      Adaabs.VarDecl ([name], decl_typ)
    ) var_list
  | SPARK_PRAGMA_DEF (sp, _) -> [PragmaDef (sp)]
  | _ -> raise (InternalTranspilerError "Unsupported function prototype")
