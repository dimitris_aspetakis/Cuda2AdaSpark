open Adaabs

type 'a visitAction = 
    SkipChildren                        (* Do not visit the children. Return
                                         * the node as it is *)
  | ChangeTo of 'a                      (* Replace the expression with the
                                         * given one *)
  | DoChildren                          (* Continue with the children of this
                                         * node. Rebuild the node on return
                                         * if any of the children changes
                                         * (use == test) *)
  | ChangeDoChildrenPost of 'a * ('a -> 'a) (* First consider that the entire
                                          * exp is replaced by the first
                                          * paramenter. Then continue with
                                          * the children. On return rebuild
                                          * the node if any of the children
                                          * has changed and then apply the
                                          * function on the node *)

class type adaabsVisitor = object
  method vtype : ada_type -> ada_type visitAction
  method vexpr : ada_expr -> ada_expr visitAction
  method vstmnt : ada_stmnt -> ada_stmnt list visitAction
  method vblock : ada_block -> ada_block visitAction
  method vdef : ada_definition -> ada_definition list visitAction
  method vdeclblock : ada_decl_block -> ada_decl_block visitAction
end

class nopAdaabsVisitor : adaabsVisitor = object
  method vtype (t : ada_type) = DoChildren
  method vexpr (e : ada_expr) = DoChildren
  method vstmnt (s: ada_stmnt) = DoChildren
  method vblock (b: ada_block) = DoChildren
  method vdef (d: ada_definition) = DoChildren
  method vdeclblock (b: ada_decl_block) = DoChildren
end


(* Map but try not to copy the list unless necessary *)
let rec mapNoCopy (f: 'a -> 'a) = function
  | [] -> []
  | (i :: resti) as li ->
      let i' = f i in
      let resti' = mapNoCopy f resti in
      if i' != i || resti' != resti then i' :: resti' else li
        
let rec mapNoCopyList (f: 'a -> 'a list) = function
  | [] -> []
  | (i :: resti) as li ->
      let il' = f i in
      let resti' = mapNoCopyList f resti in
      match il' with
        [i'] when i' == i && resti' == resti -> li
      | _ -> il' @ resti'
                     
let doVisit (vis: adaabsVisitor)
    (startvisit: 'a -> 'a visitAction)
    (children: adaabsVisitor -> 'a -> 'a)
    (node: 'a) : 'a =
  let action = startvisit node in
  match action with
  | SkipChildren -> node
  | ChangeTo node' -> node'
  | _ ->
      let nodepre = match action with
      | ChangeDoChildrenPost (node', _) -> node'
      | _ -> node
      in
      let nodepost = children vis nodepre in
      match action with
      | ChangeDoChildrenPost (_, f) -> f nodepost
      | _ -> nodepost

(* A visitor for lists *)
let doVisitList (vis: adaabsVisitor)
                (startvisit: 'a -> 'a list visitAction)
                (children: adaabsVisitor -> 'a -> 'a)
                (node: 'a) : 'a list =
  let action = startvisit node in
  match action with
  | SkipChildren -> [node]
  | ChangeTo nodes' -> nodes'
  | _ ->
      let nodespre = match action with
      | ChangeDoChildrenPost (nodespre, _) -> nodespre
      | _ -> [node]
      in
      let nodespost = mapNoCopy (children vis) nodespre in
      match action with
      | ChangeDoChildrenPost (_, f) -> f nodespost
      | _ -> nodespost



(***************************** Visitor Functions *****************************)
let rec visitAdaabsType vis typ : ada_type =
  doVisit vis vis#vtype childrenAdaType typ
and childrenAdaType vis typ = match typ with
  | Record r ->
    Record (mapNoCopy (fun (ids, (attrs, typ)) ->
      (ids, (attrs, visitAdaabsType vis typ))
    ) r)
  | Array (range_list, arr_typ, typ_var) ->
    let arr_typ' = visitAdaabsType vis arr_typ in
    if arr_typ' != arr_typ then Array (range_list, arr_typ', typ_var) else typ
  | Access acc_typ ->
    Access (visitAdaabsType vis acc_typ)
  | BaseType _ | NamedType _ -> typ

and visitAdaabsExpr vis expr : ada_expr =
  doVisit vis vis#vexpr childrenAdaExpr expr
and childrenAdaExpr vis expr = match expr with
  | UnaryExpr (un_op, un_expr) ->
    let un_expr' = visitAdaabsExpr vis un_expr in
    if un_expr' != un_expr then UnaryExpr (un_op, un_expr') else expr
  | BinaryExpr (expr1, bin_op, expr2) ->
    let expr1' = visitAdaabsExpr vis expr1 in
    let expr2' = visitAdaabsExpr vis expr2 in
    if expr1' != expr1 || expr2' != expr2 then
      BinaryExpr (expr1', bin_op, expr2')
    else expr
  | ParenExpr (child_expr) ->
    ParenExpr (visitAdaabsExpr vis child_expr)
  | CastExpr (typ, child_expr) ->
    let child_expr' = visitAdaabsExpr vis child_expr in
    let typ' = visitAdaabsType vis typ in
    if typ' != typ || child_expr' != child_expr then
      CastExpr (typ', child_expr')
    else expr
  | IndexExpr (arr_expr, idx_expr_list) ->
    let idx_expr_list' = mapNoCopy (visitAdaabsExpr vis) idx_expr_list in
    let arr_expr' = visitAdaabsExpr vis arr_expr in
    IndexExpr (arr_expr', idx_expr_list')
  | TupleExpr (expr_list) ->
    let expr_list' = mapNoCopy (visitAdaabsExpr vis) expr_list in
    if expr_list' != expr_list then TupleExpr (expr_list') else expr
  | MemberOfExpr (parent_expr, member) ->
    let parent_expr' = visitAdaabsExpr vis parent_expr in
    if parent_expr' != parent_expr then
      MemberOfExpr (parent_expr', member)
    else expr
  | CompoundExpr init_list -> CompoundExpr (
      List.map (fun (init_what, expr) ->
        (init_what, visitAdaabsExpr vis expr)
      ) init_list
    )
  | ProcedureCall (ident, params) -> 
    let params' = List.map (visitAdaabsExpr vis) params in
    ProcedureCall (ident, params')
  | KernelCall (ident, dims, params) -> 
    let params' = List.map (visitAdaabsExpr vis) params in
    KernelCall (ident, dims, params')
  | ConstantExpr _ | VarExpr _ -> expr
  | AllocExpr (typ_name, typ_args) ->
    let typ_args' = List.map (visitAdaabsExpr vis) typ_args in
    AllocExpr (typ_name, typ_args')

and visitAdaabsStmnt vis stmnt : ada_stmnt list =
  doVisitList vis vis#vstmnt childrenAdaStmnt stmnt
and childrenAdaStmnt vis stmnt =
  let ve e = visitAdaabsExpr vis e in
  match stmnt with
  | Computation (expr) ->
    let expr' = ve expr in
    if expr' != expr then Computation (expr') else stmnt
  | Block (stmnt_list) ->
    let stmnt_list' = visitAdaabsBlock vis stmnt_list in
    if stmnt_list' != stmnt_list then
      Block (stmnt_list')
    else stmnt
  | If (expr, blk, else_stmnt) ->
    let expr' = ve expr in
    let blk' = visitAdaabsBlock vis blk in
    let else_stmnt' = childrenAdaElse vis else_stmnt in
    if expr' != expr || blk' != blk || else_stmnt' != else_stmnt then
      If (expr', blk', else_stmnt')
    else stmnt
  | Return (expr) ->
    let expr' = ve expr in
    if expr' != expr then
      Return (expr')
    else stmnt
  | Definition (d) -> begin
    match visitAdaabsDefinition vis d with
      | [d'] when d' == d -> stmnt
      | [d'] -> Definition (d')
      | dl ->
        let dl' = List.map (fun d' -> Definition (d')) dl in
        Block (dl')
    end
  | Pragma (sp) -> begin
    match sp with
      | ScalarRange _ | InArgs _ | OutArgs _ | InOutArgs _ -> stmnt
      | Assume (expr) ->
        let expr' = ve expr in
        if expr' != expr then
          Pragma (Assume (expr'))
        else stmnt
      | Assert (expr) ->
        let expr' = ve expr in
        if expr' != expr then
          Pragma (Assert (expr'))
        else stmnt
      | Pre (expr) ->
        let expr' = ve expr in
        if expr' != expr then
          Pragma (Pre (expr'))
        else stmnt
      | Post (expr) ->
        let expr' = ve expr in
        if expr' != expr then
          Pragma (Post (expr'))
        else stmnt
    end
and childrenAdaElse vis else_stmnt = match else_stmnt with
  | ElseStop -> ElseStop
  | Elsif (expr, blk, else_stmnt2) ->
    let expr' = visitAdaabsExpr vis expr in
    let blk' = visitAdaabsBlock vis blk in
    let else_stmnt2' = childrenAdaElse vis else_stmnt2 in
    if expr' != expr || blk' != blk || else_stmnt2' != else_stmnt2 then
      Elsif (expr', blk', else_stmnt2')
    else else_stmnt
  | Else (blk) ->
    let blk' = visitAdaabsBlock vis blk in
    if blk' != blk then
      Else (blk')
    else else_stmnt

and visitAdaabsBlock vis blk : ada_block =
  doVisit vis vis#vblock childrenAdaBlock blk
and childrenAdaBlock vis blk =
  mapNoCopyList (visitAdaabsStmnt vis) blk

and visitAdaabsDefinition vis def : ada_definition list =
  doVisitList vis vis#vdef childrenAdaDef def
and childrenAdaDef vis def = match def with
  | FunDef (target, id, attrs, args, decl_blk, blk) ->
    let args' = List.map (fun (id, (attr_lst, typ)) ->
      let typ' = visitAdaabsType vis typ in
      (id, (attr_lst, typ'))
    ) args in
    let decl_blk' = visitAdaabsDeclBlock vis decl_blk in
    let blk' = visitAdaabsBlock vis blk in
    if args' != args || decl_blk' != decl_blk || blk' != blk then
      FunDef (target, id, attrs, args', decl_blk', blk')
    else def
  | FunDecl (target, id, attrs, args) ->
    let args' = List.map (fun (id, (attr_lst, typ)) ->
      let typ' = visitAdaabsType vis typ in
      (id, (attr_lst, typ'))
    ) args in
    if args' != args then
      FunDecl (target, id, attrs, args')
    else def
  | VarDef (ids, (attr_lst, typ), expr) ->
    let typ' = visitAdaabsType vis typ in
    let expr' = visitAdaabsExpr vis expr in
    if typ' != typ || expr' != expr then
      VarDef (ids, (attr_lst, typ'), expr')
    else def
  | VarDecl (ids, (attr_lst, typ)) ->
    let typ' = visitAdaabsType vis typ in
    if typ' != typ then
      VarDecl (ids, (attr_lst, typ'))
    else def
  | TypeDef (td) -> begin
    match td with
    | FullType (tn, typ, attrs) ->
      let typ' = visitAdaabsType vis typ in
      if typ' != typ then
        TypeDef (FullType (tn, typ', attrs))
      else def
    | SubType (tn, typ, lb, ub, attrs) ->
      let typ' = visitAdaabsType vis typ in
      if typ' != typ then
        TypeDef (SubType (tn, typ', lb, ub, attrs))
      else def
    end
  | PragmaDef (sp) -> begin
    match sp with
    | ScalarRange _ | InArgs _ | OutArgs _ | InOutArgs _ -> def
    | Assume (expr) ->
      let expr' = visitAdaabsExpr vis expr in
      if expr' != expr then
        PragmaDef (Assume (expr'))
      else def
    | Assert (expr) ->
      let expr' = visitAdaabsExpr vis expr in
      if expr' != expr then
        PragmaDef (Assert (expr'))
      else def
    | Pre (expr) ->
      let expr' = visitAdaabsExpr vis expr in
      if expr' != expr then
        PragmaDef (Pre (expr'))
      else def
    | Post (expr) ->
      let expr' = visitAdaabsExpr vis expr in
      if expr' != expr then
        PragmaDef (Post (expr'))
      else def
    end

and visitAdaabsDeclBlock vis decl_blk : ada_decl_block =
  doVisit vis vis#vdeclblock childrenAdaDeclBlock decl_blk
and childrenAdaDeclBlock vis decl_blk =
  mapNoCopyList (visitAdaabsDefinition vis) decl_blk

and visitAdaabsDefList vis def_list : ada_definition list =
  List.flatten (List.map (visitAdaabsDefinition vis) def_list)
