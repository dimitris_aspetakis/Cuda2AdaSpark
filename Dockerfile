FROM debian:buster-slim
RUN apt-get update && apt-get install -y --no-install-recommends vim nano make g++ git gcc cmake ocaml camlp4 ocamlbuild perl autoconf wget ca-certificates
RUN apt-get clean

RUN wget http://download.camlcity.org/download/findlib-1.9.1.tar.gz \
    && tar -xvf findlib-1.9.1.tar.gz \
    && cd findlib-1.9.1 \
    && ./configure \
    && make all opt \
    && make install \
    && cd .. \
    && rm -rf findlib-1.9.1.tar.gz findlib-1.9.1

RUN wget https://github.com/backtracking/ocamlgraph/releases/download/v1.8.8/ocamlgraph-1.8.8.tar.gz \
    && tar -xvf ocamlgraph-1.8.8.tar.gz \
    && cd ocamlgraph-1.8.8 \
    && ./configure \
    && make \
    && make install \
    && make install-findlib \
    && cd .. \
    && rm -rf ocamlgraph-1.8.8.tar.gz ocamlgraph-1.8.8

RUN git clone https://gitlab.bsc.es/dimitris_aspetakis/Cuda2AdaSpark.git \
    && cd Cuda2AdaSpark \
    && cd cil \
    && ./configure \
    && make \
    && cd ../transpiler_passes \
    && mkdir build \
    && cd build \
    && cmake .. \
    && cd ../.. \
    && make

ENV PATH="/Cuda2AdaSpark/cil:${PATH}"

CMD /bin/bash
